Pod::Spec.new do |s|

  s.name         = "IlhasoftCore"
  s.version      = "0.0.9"
  s.summary      = "Easy iOS development"

  s.description  = <<-DESC
                   Libs to improve iOS development
                   DESC

  s.homepage     = "https://bitbucket.org/ilhasoft/ilhasoft-core-ios"
  s.license      = { :type => 'MIT', :file => 'LICENSE' }
  s.author       = { "Daniel Amaral" => "daniel@ilhasoft.com.br" }
  s.social_media_url   = "https://twitter.com/danielamarall"

  s.platform     = :ios
  s.ios.deployment_target = '9.0'
  s.source       = { :git => "https://bitbucket.org/ilhasoft/ilhasoft-core-ios.git", :branch => "swift3"}
  s.xcconfig = { 'LIBRARY_SEARCH_PATHS' => '"$(PODS_ROOT)/IlhasoftCore"' }
  s.requires_arc = true
  s.framework = 'UIKit'
  s.framework = 'AVFoundation'

  s.resources    = "Resources/*"
  s.source_files = 'Classes/**/*'

  s.resource_bundles = {
    'IlhasoftCore' => ['Pod/Resources/*.png']
  }

  #s.public_header_files = 'Pod/Classes/**/*.h'
  s.dependency 'Proposer'
  s.dependency 'SDWebImage', '3.8.2'
  s.dependency 'TPKeyboardAvoiding'
  s.dependency 'AlamofireObjectMapper'
  s.dependency 'Alamofire'
  s.dependency 'MBProgressHUD'

end
