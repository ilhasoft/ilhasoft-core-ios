![](https://img.shields.io/cocoapods/v/IlhasoftCore.svg?label=Current%20Release)
[![Cocoapods badge](https://img.shields.io/cocoapods/p/IlhasoftCore.svg?maxAge=2592000?style=flat-square)]()

# Ilhasoft Core Library

The Ilhasoft Core Library is a library written in `Swift` designed to help sharing code among projects. This library is intented to be included in all Ilhasoft projects and can be used in both `Swift` and `Objective-C` projects.

## Usage

You can check examples of usage in the library's [wiki](https://bitbucket.org/ilhasoft/ilhasoft-core-ios/wiki/Home).
