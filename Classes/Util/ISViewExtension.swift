//
//  ISViewExtension.swift
//  Pods
//
//  Created by Daniel Amaral on 08/07/16.
//
//

import UIKit

extension UIView {

    public func roundCorners(corners: UIRectCorner, withRadius radius: Double) {
        self.setNeedsLayout()
        self.layoutIfNeeded()
        let maskPath: UIBezierPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let maskLayer: CAShapeLayer = CAShapeLayer()
        maskLayer.frame = self.bounds
        maskLayer.path = maskPath.cgPath
        self.layer.mask = maskLayer
    }

}
