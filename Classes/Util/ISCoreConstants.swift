//
//  ISCoreConstants.swift
//  IlhasoftCore
//
//  Created by Dielson Sales de Carvalho on 29/04/16.
//  Copyright © 2016 ilhasoft. All rights reserved.
//

import UIKit

open class ISCoreConstants: NSObject {

    fileprivate static var instanceBundle: Bundle?
    fileprivate static let localizableFile = "Localizable"

    open static let isIpad = UIDevice.current.userInterfaceIdiom == .pad
    
    open static func frameworkBundle() -> Bundle {
        if let bundle = instanceBundle {
            return bundle
        } else {
            instanceBundle = Bundle(for: ISCoreConstants.self)
            return instanceBundle!
        }
    }

    open static func localizedStringForKey(_ key: String) -> String {
        return ISCoreConstants.frameworkBundle().localizedString(forKey: key, value: "Strings file not found", table: localizableFile)
    }

}
