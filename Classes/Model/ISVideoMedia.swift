//
//  ISVideoPhoneMedia.swift
//  Pods
//
//  Created by Daniel Amaral on 14/12/16.
//
//

import UIKit

public class ISVideoMedia: ISMedia {

    public var path:String!
    public var thumbnailImage:UIImage!
    
}
