//
//  ISImageMedia.swift
//  Pods
//
//  Created by Daniel Amaral on 11/10/16.
//
//

import UIKit

public class ISImageMedia: ISMedia {
    
    public var image:UIImage!
    
}
