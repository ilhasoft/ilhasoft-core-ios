//
//  ISLocalMedia.swift
//  Pods
//
//  Created by Daniel Amaral on 11/10/16.
//
//

import UIKit

public class ISFileMedia: ISMedia {
    public var path:String!
    public var metadata:NSDictionary!
}
