//
//  ISAlertMessages.swift
//  IlhasoftCore
//
//  Created by Dielson Sales on 07/07/16.
//  Copyright © 2016 ilhasoft. All rights reserved.
//

import UIKit

open class ISAlertMessages: NSObject {

    open static func displaySimpleMessage(_ message: String, fromController controller: UIViewController) {
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alertController.addAction(ISAlertMessages.createOkAction())
        controller.present(alertController, animated: true, completion: { _ in })
    }

    open static func displayMessage(_ message: String, withTitle title: String, fromController controller: UIViewController) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(ISAlertMessages.createOkAction())
        controller.present(alertController, animated: true, completion: { _ in })
    }

    open static func displayErrorMessage(_ message: String, fromController controller: UIViewController) {
        ISAlertMessages.displayMessage(message, withTitle: ISCoreConstants.localizedStringForKey("error_title"), fromController: controller)
    }

    // MARK: Private methods

    fileprivate static func createOkAction() -> UIAlertAction {
        return UIAlertAction(title: "Ok", style: .default, handler: nil)
    }
}
