//
//  ISSocialAccount.swift
//  Pods
//
//  Created by Daniel Amaral on 22/11/16.
//
//

import UIKit
import Accounts
import Social

public class ISSocialAccount: NSObject {
    
    public class func loginWithFacebook(appId:String, fields:String, completion:@escaping (_ data:NSDictionary?,_ oauthToken:String?) -> Void ) {
        
        let accountStore = ACAccountStore()
        let accountType  = accountStore.accountType(withAccountTypeIdentifier: ACAccountTypeIdentifierFacebook)
        
        accountStore.requestAccessToAccounts(with: accountType, options: [ACFacebookAppIdKey:appId, ACFacebookPermissionsKey: ["public_profile","email"], ACFacebookAudienceKey: ACFacebookAudienceOnlyMe], completion: {success, error in
            
            if let error = error {
                print(error)
                completion(nil,nil)
                return
            }
            
            if let facebookAccount = accountStore.accounts(with: accountType).first as? ACAccount {
                
                let fbCredential = facebookAccount.credential
                let accessToken = fbCredential?.oauthToken
                print("Facebook Access Token: \(accessToken)")
                
                loadFacebookProfileInfo(fields: fields,account: facebookAccount, onComplete: { (dictionary) in
                    completion(dictionary!,accessToken)
                })
                
            } else {
                completion(nil,nil)
            }
        })
    }
    
    private class func loadFacebookProfileInfo(fields:String,account:ACAccount, onComplete: @escaping (NSDictionary?) -> ()) {
        
        let meUrl = NSURL(string: "https://graph.facebook.com/me")!
        
        let slRequest = SLRequest(forServiceType: SLServiceTypeFacebook,
                                  requestMethod: SLRequestMethod.GET,
                                  url: meUrl as URL!, parameters: ["fields":fields])
        
        slRequest?.account = account
        
        slRequest?.perform(handler: { (data, response, error) in
            
            if let data = data {
                do {
                    let meData = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    onComplete(meData)
                }
                catch{
                    print(error)
                    onComplete(nil)
                }
            }else {
                onComplete(nil)
            }
        })
    }
    
}
