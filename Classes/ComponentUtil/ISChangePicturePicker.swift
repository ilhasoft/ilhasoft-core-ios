//
//  ISChangePicturePicker.swift
//  Pods
//
//  Created by Daniel Amaral on 07/06/16.
//
//

import Proposer
import UIKit
import MobileCoreServices

public protocol ISChangePicturePickerDelegate {
    func mediaDidRemove(imageView:UIImageView)
}


public class ISChangePicturePicker: NSObject {
    
    var presentViewController:UIViewController!
    
    var imagePickerProtocol:protocol<UIImagePickerControllerDelegate, UINavigationControllerDelegate>!
    var delegate:ISChangePicturePickerDelegate?
    var mediaSources:[ISMediaSource]?
    var videoMaximumDuration:Int? = 10
    var view:UIView!
    
    public init(parentViewController:UIViewController!,imagePickerProtocol:UIImagePickerControllerDelegate&UINavigationControllerDelegate!,view:UIView?) {
        self.presentViewController = parentViewController
        self.imagePickerProtocol = imagePickerProtocol
        self.view = view
        
        super.init()
    }
    
    public func changePicture() {
        
        if mediaSources == nil {
            mediaSources = ISMediaSource.allMediaSources
        }
        
        proposeToAccess(PrivateResource.photos, agreed: {
            
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self.imagePickerProtocol
            
            let alert = UIAlertController(title: nil, message: ISCoreConstants.localizedStringForKey("choice_option"), preferredStyle: UIAlertControllerStyle.actionSheet)
            
            if let _ = self.mediaSources!.index(of: .Gallery) {
                alert.addAction(UIAlertAction(title: ISCoreConstants.localizedStringForKey("choice_gallery"), style: UIAlertActionStyle.default, handler: { (alertAction) in
                    imagePicker.allowsEditing = false;
                    imagePicker.sourceType = .photoLibrary
                    self.presentViewController.present(imagePicker, animated: true, completion: {})
                }))
            }
            
            if let _ = self.mediaSources!.index(of: .Camera) {
                alert.addAction(UIAlertAction(title: ISCoreConstants.localizedStringForKey("choice_take_picture"), style: UIAlertActionStyle.default, handler: { (alertAction) in
                    imagePicker.sourceType = .camera
                    imagePicker.cameraCaptureMode = UIImagePickerControllerCameraCaptureMode.photo
                    imagePicker.showsCameraControls = true
                    imagePicker.allowsEditing = true
                    self.presentViewController.present(imagePicker, animated: true, completion: {})
                }))
            }
            
            if let _ = self.mediaSources!.index(of: .Video) {
                alert.addAction(UIAlertAction(title: ISCoreConstants.localizedStringForKey("choice_video"), style: UIAlertActionStyle.default, handler: { (alertAction) in
                    imagePicker.sourceType = .camera
                    imagePicker.mediaTypes = [kUTTypeMovie as String]
                    imagePicker.videoQuality = .type640x480
                    imagePicker.videoMaximumDuration = Double(self.videoMaximumDuration!)
                    imagePicker.allowsEditing = false
                    self.presentViewController.present(imagePicker, animated: true, completion: {})
                }))
            }
            
            if let _ = self.mediaSources!.index(of: .Audio) {
                alert.addAction(UIAlertAction(title: ISCoreConstants.localizedStringForKey("choice_video"), style: UIAlertActionStyle.default, handler: { (alertAction) in
                    
                }))
            }
            
            if let imageView = self.view as? UIImageView, imageView.image != nil && imageView.tag != 500 {
                alert.addAction(UIAlertAction(title: ISCoreConstants.localizedStringForKey("choice_remove"), style: UIAlertActionStyle.default, handler: { (alertAction) in
                    imageView.image = nil
                    self.delegate?.mediaDidRemove(imageView: imageView)
                }))
            }
            
            alert.addAction(UIAlertAction(title: ISCoreConstants.localizedStringForKey("choice_cancel"), style: UIAlertActionStyle.cancel, handler: { (alertAction) in }))
            
            
            if ISCoreConstants.isIpad {
                alert.modalPresentationStyle = UIModalPresentationStyle.popover
                alert.popoverPresentationController!.sourceView = self.view
                alert.popoverPresentationController!.sourceRect = self.view.bounds
            }
            
            self.presentViewController.present(alert, animated: true, completion: {})
            
        }, rejected: {
            self.presentViewController.alertNoPermissionToAccess(PrivateResource.photos)
        })
    }
    
}
