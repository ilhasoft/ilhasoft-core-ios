//
//  AudioView.swift
//  ureport
//
//  Created by Daniel Amaral on 16/03/16.
//  Copyright © 2016 ilhasoft. All rights reserved.
//

import UIKit
import AVFoundation

public protocol ISAudioViewDelegate {
    func finishRecord()
}

public class ISAudioView: UIView, AVAudioPlayerDelegate, ISAudioRecorderManagerDelegate {

    var player:AVAudioPlayer!
    var audioRemoteURL:String?
    
    @IBOutlet weak var slider: UISlider!    
    @IBOutlet weak var btPlay: UIButton!
    @IBOutlet weak var lbCurrentTime: UILabel!
    @IBOutlet weak var lbMaxTime: UILabel!
    
    var isRecording:Bool!
    var delegate:ISAudioRecorderViewControllerDelegate?
    var audioViewdelegate:ISAudioViewDelegate?
    
    var startTimeRecording:CFAbsoluteTime!
    var startTimePlayBack:CFAbsoluteTime!
    var timerRecording:Timer!
    var timerPlayback:Timer!
    
    var recordedSeconds = 0
    let maximumTime = 50
    var audioMedia:ISAudioMedia?
    var audioRecorder:ISAudioRecorderManager!
    
    var playAudioImmediately:Bool!
    
    override public func awakeFromNib() {
        isRecording = false
    }
    
    //MARK: AudioRecorderManagerDelegate
    
    public func audioRecorderDidFinish(_ path: String) {
        
        audioMedia = ISAudioMedia()
        audioMedia!.path = path
        
        setupAudioPlayerWithURL(ISAudioRecorderManager.outputURLFile)
    }
    
    //MARK: Class Methods
    
    public func playAudioImmediately(_ audioURL:String,showPreloading:Bool) {
        if showPreloading == true {

        }
        ISDownloader.download(URL(string: audioURL)!) { (data) -> Void in
            if let data = data {
                DispatchQueue.main.async(execute: {
                    self.setupAudioPlayerWithURL(URL(string: ISFileUtil.writeAudioFile(data))!)
                })
            }else{
                print("playAudioImmediately error")
            }
        }
    }
    
    public func setupSliderWithPlayMode() {
        
        recordedSeconds = recordedSeconds == 0 ? Int(player.duration) : recordedSeconds
        
        self.btPlay.isEnabled = true
        self.btPlay.setImage(UIImage(named: "play-icon", in: Bundle(for: self.classForCoder), compatibleWith: nil), for: UIControlState.normal)
        
        slider.value = 0
        slider.minimumValue = 0
        slider.maximumValue = Float(recordedSeconds)
        
        self.lbCurrentTime.text = "00:00"
        
        self.lbMaxTime.text = getDurationString(recordedSeconds)
        
        var thumbImage = UIImage(named: "circle", in: Bundle(for: self.classForCoder), compatibleWith: nil)
        
        if var thumbImage = thumbImage {
            let imageSize = CGSize(width: 23, height: 23)
            
            UIGraphicsBeginImageContextWithOptions(imageSize, false, 0.0)
            thumbImage.draw(in: CGRect(x: 0, y: 0, width: imageSize.width, height: imageSize.height))
            thumbImage = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
            
            slider.setThumbImage(thumbImage, for: UIControlState())
        }
    }
    
    public func setupAudioPlayerWithURL(_ url:URL) {
        do {
            player = try AVAudioPlayer(contentsOf: url)
            
            player.delegate = self
            player.prepareToPlay()
            player.volume = 1.0
            
            setupSliderWithPlayMode()
            
            if audioRemoteURL != nil {
                play()
            }
            
        }catch let error as NSError {
            self.btPlay.isEnabled = false
            print("Error on load Audio, try again.")
            print(error.localizedDescription)
        }
    }
    
    public func play() {
        if !player.isPlaying {
            NotificationCenter.default.post(name: Notification.Name(rawValue: "didStartPlaying"), object: self)
            self.btPlay.setImage(UIImage(named: "ic_pause_orange", in: Bundle(for: self.classForCoder), compatibleWith: nil), for: UIControlState.normal)
            startTimePlayBack = CFAbsoluteTimeGetCurrent()
            player.play()
            timerPlayback = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerCheckOnPlayBlack), userInfo: nil, repeats: true)
            timerPlayback.fire()
        }else{
            self.btPlay.setImage(UIImage(named: "play-icon", in: Bundle(for: self.classForCoder), compatibleWith: nil), for: UIControlState.normal)
            timerPlayback.invalidate()
            player.pause()
        }
    }
    
    public func setupSlider() {
        if isRecording == false {
            slider.setThumbImage(UIImage(), for: UIControlState.normal)
            slider.value = 0
            slider.isContinuous = true
            slider.minimumValue = 0
            slider.maximumValue = Float(maximumTime)
        }
    }
    
    public func timerCheckOnRecording() {
        recordedSeconds = Int(CFAbsoluteTimeGetCurrent() - startTimeRecording)
        
        UIView.animate(withDuration: 1.5, animations: {
            self.slider.setValue(Float(self.recordedSeconds), animated:true)
        })
        
        self.lbCurrentTime.text = getDurationString(recordedSeconds)
        
        if recordedSeconds == maximumTime {
            needFinishRecord()
            return
        }
        
    }
    
    public func needFinishRecord() {
        if let delegate = audioViewdelegate {
            delegate.finishRecord()
        }
        isRecording = false
        timerRecording.invalidate()
        audioRecorder.stopRecording()
        setupSlider()
    }
    
    public func timerCheckOnPlayBlack() {
        
        DispatchQueue.main.async(execute: {
            if self.player.currentTime == 0 {
                self.slider.value = 0
            }else{
                UIView.animate(withDuration: 1.5, animations: {
                    self.slider.setValue(Float(self.player.currentTime), animated:true)
                })
            }
        })
        
        self.lbCurrentTime.text = getDurationString(Int(self.player.currentTime))
        
        if Int(self.player.currentTime) == recordedSeconds - 1 {
            timerPlayback.invalidate()
            setupSliderWithPlayMode()
            return
        }
        
    }
    
    public func getDurationString(_ seconds:Int) -> String{
        var seconds = seconds
        let minutes = (seconds % 3600) / 60
        seconds = seconds % 60
        return "\(ISAudioView.getTwoDigitString(minutes)):\(ISAudioView.getTwoDigitString(seconds))"
    }
    
    public class func getTwoDigitString(_ number:Int) -> String{
        if number == 0 {
            return "00"
        }
        
        if number / 10 == 0 {
            return "0\(number)"
        }
        
        return String(number)
        
    }
    
    //MARK: UI Events
    
    @IBAction func sliderMoved(_ sender: UISlider) {
        
        self.lbCurrentTime.text = getDurationString(Int(self.player.currentTime))
        
        if player.isPlaying == true {
            player.pause()
        }
        
        player.currentTime = TimeInterval(round(slider.value))
        play()
    }
    
    //MARK: Button Events
    
    @IBAction func btPlayTapped() {
        play()
    }
    
}
