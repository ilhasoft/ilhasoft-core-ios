//
//  URAudioRecorderManager.swift
//  ureport
//
//  Created by Daniel Amaral on 17/02/16.
//  Copyright © 2016 ilhasoft. All rights reserved.
//

import UIKit
import AVFoundation

public protocol ISAudioRecorderManagerDelegate {
    func audioRecorderDidFinish(_ path:String)
}

public class ISAudioRecorderManager: NSObject,  AVAudioRecorderDelegate {

    static let outputURLFile = URL(fileURLWithPath: ISFileUtil.outPutURLDirectory.appendingPathComponent("audio.m4a"))
    
    var recorder:AVAudioRecorder!
    var delegate:ISAudioRecorderManagerDelegate?
    
    static let recordSettings = [
        AVFormatIDKey: NSNumber(value: kAudioFormatMPEG4AAC as UInt32),
        AVEncoderAudioQualityKey : AVAudioQuality.max.rawValue,
        AVNumberOfChannelsKey: 1,
        AVSampleRateKey : 8000.0
    ] as [String : Any]
    
    public func startAudioRecord() {
        
        do {
            
            ISFileUtil.removeFile(ISAudioRecorderManager.outputURLFile)
            
            recorder = try AVAudioRecorder(url: ISAudioRecorderManager.outputURLFile, settings: ISAudioRecorderManager.recordSettings)
            recorder.delegate = self
            recorder.isMeteringEnabled = true
            recorder.prepareToRecord() // creates/overwrites the file at soundFileURL
            
            do {
                try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayAndRecord, with: AVAudioSessionCategoryOptions.defaultToSpeaker)
                try AVAudioSession.sharedInstance().setActive(true)
                recorder.record()
            }catch let error as NSError {
                print(error)
            }
            

        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    public func stopRecording() {
        recorder.stop()
        closeSession()
    }
    
    public func closeSession() {
        do {
            try AVAudioSession.sharedInstance().setActive(false)
        }catch let error as NSError {
            print(error)
        }
    }
    
    //MARK: AVAudioRecorderDelegate
    
    public func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if let delegate = self.delegate {
            delegate.audioRecorderDidFinish(ISAudioRecorderManager.outputURLFile.path)
        }
    }
    
    public func audioRecorderEncodeErrorDidOccur(_ recorder: AVAudioRecorder, error: Error?) {
        if let error = error {
            print(error.localizedDescription)
        }
    }
    
}
